# README #

Download the Screensaver/Screener/bin/Debug/Screener.exe file and rename it to Screener.src to make it a screensaver file.
Then right click on the file, and install, or test.


### What is this repository for? ###

This is a sample code to develop a screensaver on Visual Studio(VS) 2013 for Windows OS. The language used is C#.

### How do I get set up? ###

Download the VS Solution file TextScreener.sln and the source code folder beside it.
Open the .sln file in VS, and you are good to go.

### Contribution guidelines ###

Any further query can be sent to me at plus91amit@gmail.com