﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.Drawing.Text;

namespace Screener
{
    public partial class ScreenSaverForm : Form
    {
        public ScreenSaverForm()
        {
            InitializeComponent();
        }
        public ScreenSaverForm(Rectangle bounds)
        {
            InitializeComponent();
            this.Bounds = bounds;
        }

        [DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [DllImport("user32.dll")]
        static extern int SetWindowLong(IntPtr hWnd, int nIndex, IntPtr dwNewLong);

        [DllImport("user32.dll", SetLastError = true)]
        static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        static extern bool GetClientRect(IntPtr hWnd, out Rectangle lpRect);

        public ScreenSaverForm(IntPtr PreviewWndHandle)
        {
            InitializeComponent();

            // Set the preview window as the parent of this window
            SetParent(this.Handle, PreviewWndHandle);

            // Make this a child window so it will close when the parent dialog closes
            // GWL_STYLE = -16, WS_CHILD = 0x40000000
            SetWindowLong(this.Handle, -16, new IntPtr(GetWindowLong(this.Handle, -16) | 0x40000000));

            // Place our window inside the parent
            Rectangle ParentRect;
            GetClientRect(PreviewWndHandle, out ParentRect);
            Size = ParentRect.Size;
            Location = new Point(0, 0);

            // Make text smaller
            textLabel.Font = new System.Drawing.Font("Arial", 6);

            previewMode = true;
        }

        FontFamily[] fontList;
        private bool previewMode = false;
        private Random rand = new Random();
        private void ScreenSaverForm_Load(object sender, EventArgs e)
        {
            // Use the string from the Registry if it exists
            RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Demo_ScreenSaver");

            if (key == null)
                textLabel.Text = "C# Screen Saver";
            else
                textLabel.Text = (string)key.GetValue("text");

            Cursor.Hide();
            TopMost = true;

            InstalledFontCollection ifc = new InstalledFontCollection();
            fontList = ifc.Families;
            
            moveTimer.Interval = 2000;
            moveTimer.Tick += new EventHandler(moveTimer_Tick);
            moveTimer.Start();

            //bgTimer.Interval = 1000;
            //bgTimer.Start();

        }

        private void moveTimer_Tick(Object sender, System.EventArgs e) {
            try
            {
                textLabel.Left = rand.Next(Math.Max(1, Bounds.Width - textLabel.Width));
                textLabel.Top = rand.Next(Math.Max(1, Bounds.Height - textLabel.Height));
                //textLabel.Text = fontList[rand.Next(fontList.Length)].GetName(0);
                textLabel.ForeColor = Color.FromArgb(rand.Next(255), rand.Next(255), rand.Next(255));
                float fontSize = rand.Next(14, 92);
                FontFamily font = fontList[rand.Next(fontList.Length)];
                textLabel.Font = new System.Drawing.Font(font, fontSize);
            }
            catch (Exception)
            {
                //throw;
            }
            
        }
        private void ScreenSaverForm_MouseClick(object sender, MouseEventArgs e)
        {
            EndApp();
        }

        private void ScreenSaverForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            EndApp();
        }

        private Point mouseLocation;
        
        private void ScreenSaverForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (!mouseLocation.IsEmpty)
            {
                // Terminate if mouse is moved a significant distance
                if (Math.Abs(mouseLocation.X - e.X) > 5 ||
                    Math.Abs(mouseLocation.Y - e.Y) > 5)
                    EndApp();
            }

            // Update current mouse location
            mouseLocation = e.Location;

        }

        private void EndApp() { 
            if(!previewMode)
                Application.Exit();
        }

        private void bgTimer_Tick(object sender, EventArgs e)
        {
            //this.BackColor = Color.FromArgb(rand.Next(255), rand.Next(255), rand.Next(255));
        }
    }
}
