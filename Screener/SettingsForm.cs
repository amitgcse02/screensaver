﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Screener
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();
            LoadSettings();
        }

        private void SaveSettings() {
            RegistryKey key = Registry.CurrentUser.CreateSubKey("SOFTWARE\\Demo_ScreenSaver");
            key.SetValue("text", textBox.Text);
        }

        private void LoadSettings()
        {
            RegistryKey key = Registry.CurrentUser.CreateSubKey("SOFTWARE\\Demo_ScreenSaver");
            if (key == null)
            {
                textBox.Text = "C# Screen Saver";
            }
            else
            {
                textBox.Text = (string)key.GetValue("text");  
            }
        }

       private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

       private void okButton_Click(object sender, EventArgs e)
       {
           SaveSettings();
           Close();
       }

     
    }
}
